# Generated by Django 3.1.4 on 2020-12-13 21:28

import django.contrib.postgres.fields.jsonb
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0006_auto_20201213_2128'),
    ]

    operations = [
        migrations.AlterField(
            model_name='request',
            name='body',
            field=models.TextField(null=True),
        ),
        migrations.AlterField(
            model_name='request',
            name='extras',
            field=django.contrib.postgres.fields.jsonb.JSONField(null=True),
        ),
    ]
