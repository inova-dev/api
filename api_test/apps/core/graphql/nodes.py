from graphene import relay
from graphene_django import DjangoObjectType
# Models
from ..models import Log, Request


class LogNode(DjangoObjectType):
    class Meta:
        model = Log
        interfaces = [relay.Node]
        filter_fields = {}


class RequestNode(DjangoObjectType):
    class Meta:
        model = Request
        interfaces = [relay.Node]
        filter_fields = {}
