import graphene
from graphene_django.filter import DjangoFilterConnectionField
from graphene import ObjectType
# Nodes
from .nodes import LogNode, RequestNode
# Types
from .types import GenericRequestType
# Enums
from .enums import RequestType, Method, ResponseType, OnError
# Resolvers
from .resolvers import resolve_direct_request


class Query(ObjectType):
    all_logs = DjangoFilterConnectionField(LogNode)
    all_requests = DjangoFilterConnectionField(RequestNode)
    direct_request = graphene.Field(GenericRequestType,
                                    url=graphene.String(required=True),
                                    request_type=graphene.String(RequestType),
                                    method=graphene.String(Method, required=True),
                                    response_type=graphene.String(ResponseType),
                                    extras=graphene.JSONString(),
                                    body=graphene.JSONString(),
                                    headers=graphene.JSONString(),
                                    default=graphene.String(),
                                    on_error=graphene.String(OnError),
                                    resolver=resolve_direct_request)
