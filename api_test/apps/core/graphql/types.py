from graphene import ObjectType
from graphene.types.generic import GenericScalar


class GenericRequestType(ObjectType):
    data = GenericScalar()
