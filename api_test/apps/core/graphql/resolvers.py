import requests
# Enums
from .enums import RequestType, Method, ResponseType
# Models
from ..models import Log, Request


def resolve_direct_request(cls, info, **kwargs):
    url = kwargs.get('url')
    request_type = kwargs.get('request_type')
    if request_type == RequestType.REST.value:
        return handle_rest(url, request_type, kwargs)
    if request_type == RequestType.GRAPHQL:
        pass
    if request_type == RequestType.SOAP:
        pass
    if request_type == RequestType.WEB_SCRAPPING:
        return handle_web_scrapping(url, kwargs)
    return None


def handle_rest(url, request_type, kwargs):
    method = kwargs.get('method')
    if method == Method.GET.value:
        return handle_rest_get(url, request_type, method, kwargs)
    elif method == Method.POST.value:
        return handle_rest_post(url, request_type, method, kwargs)
    elif method == Method.PUT.value:
        return handle_rest_put(url, request_type, method, kwargs)
    elif method == Method.PATCH.value:
        return handle_rest_patch(url, request_type, method, kwargs)
    elif method == Method.DELETE.value:
        return handle_rest_delete(url, request_type, method, kwargs)
    else:
        return {
            'data': 'method Error'
        }


def handle_rest_get(url, request_type, method, kwargs):
    response_type = kwargs.get('response_type')
    extras = kwargs.get('extras')
    response = requests.get(url)
    Log.objects.create(http_status=response.status_code, url=url)
    response = response.json()
    Request.objects.create(request_type=request_type, method=method, response_type=response_type, extras=extras)
    if response_type == ResponseType.COMPLETE_RESPONSE.value:
        return {
            'data': response
        }
    elif response_type == ResponseType.INDEX.value:
        index = extras.get('index')
        return {
            'data': response[index]
        }
    elif response_type == ResponseType.INDEX_KEY.value:
        index = extras.get('index')
        key = extras.get('key')
        return {
            'data': response[index][key]
        }
    else:
        return {
            'data': 'responseType Error'
        }


def handle_rest_post(url, request_type, method, kwargs):
    response_type = kwargs.get('response_type')
    payload = kwargs.get('body')
    response = requests.post(url, data=payload)
    Log.objects.create(http_status=response.status_code, url=url)
    response = response.json()
    Request.objects.create(request_type=request_type, method=method, response_type=response_type, body=payload)
    if response_type == ResponseType.COMPLETE_RESPONSE.value:
        return {
            'data': response
        }
    else:
        return {
            'data': 'responseType Error'
        }


def handle_rest_put(url, request_type, method, kwargs):
    response_type = kwargs.get('response_type')
    payload = kwargs.get('body')
    response = requests.put(url, data=payload)
    Log.objects.create(http_status=response.status_code, url=url)
    response = response.json()
    Request.objects.create(request_type=request_type, method=method, response_type=response_type, body=payload)
    if response_type == ResponseType.COMPLETE_RESPONSE.value:
        return {
            'data': response
        }
    else:
        return {
            'data': 'responseType Error'
        }


def handle_rest_patch(url, request_type, method, kwargs):
    response_type = kwargs.get('response_type')
    payload = kwargs.get('body')
    response = requests.patch(url, data=payload)
    Log.objects.create(http_status=response.status_code, url=url)
    response = response.json()
    Request.objects.create(request_type=request_type, method=method, response_type=response_type, body=payload)
    if response_type == ResponseType.COMPLETE_RESPONSE.value:
        return {
            'data': response
        }
    else:
        return {
            'data': 'responseType Error'
        }


def handle_rest_delete(url, request_type, method, kwargs):
    response_type = kwargs.get('response_type')
    response = requests.delete(url)
    Log.objects.create(http_status=response.status_code, url=url)
    Request.objects.create(request_type=request_type, method=method, response_type=response_type)
    if response_type == ResponseType.COMPLETE_RESPONSE.value:
        return {
            'data': response.status_code
        }
    else:
        return {
            'data': 'responseType Error'
        }


# TODO
def handle_graphql():
    pass


# TODO
def handle_soap():
    pass


def handle_web_scrapping(url, kwargs):
    pass
