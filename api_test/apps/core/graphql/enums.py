import graphene


class RequestType(graphene.Enum):
    REST = 'REST'
    GRAPHQL = 'GRAPHQL'
    SOAP = 'SOAP'
    WEB_SCRAPPING = 'WEB_SCRAPPING'


class Method(graphene.Enum):
    GET = 'GET'
    POST = 'POST'
    PUT = 'PUT'
    PATCH = 'PATCH'
    DELETE = 'DELETE'


class ResponseType(graphene.Enum):
    COMPLETE_RESPONSE = 'COMPLETE_RESPONSE'
    INDEX = 'INDEX'
    INDEX_KEY = 'INDEX_KEY'
    OPERATION = 'OPERATION'


class OnError(graphene.Enum):
    NULL = 'NULL'
    RETRY_X_TIMES = 'RETRY_X_TIMES'
