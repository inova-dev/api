from django.db import models
from django.contrib.postgres.fields import JSONField


class Request(models.Model):
    request_type = models.CharField(max_length=200)  # REST | GRAPHQL | SOAP | SCRAPING
    method = models.CharField(max_length=200)   # GET POST PUT PATCH DELETE
    response_type = models.CharField(max_length=200)
    extras = JSONField(null=True)  # Esto es un JSONFIELD
    body = models.TextField(null=True)
    headers = JSONField(null=True)  # Esto es un JSONFIELD
    default = JSONField(null=True)  # Esto es un JSONFIELD
    on_error = models.CharField(max_length=200)
    chain_request = models.ForeignKey('self', null=True, on_delete=models.CASCADE)

    def __str__(self):
        return '{}:{}'.format(self.type, self.method)


class Log(models.Model):
    http_status = models.PositiveSmallIntegerField()
    url = models.TextField()
    date = models.DateTimeField(auto_now_add=True)
    request = models.ForeignKey(Request, null=True, on_delete=models.SET_NULL)

    def __str__(self):
        return self.http_status
