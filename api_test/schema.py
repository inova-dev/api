from .apps.core.graphql import schema
import graphene


class Query(schema.Query, graphene.ObjectType):
    pass


schema = graphene.Schema(query=Query)
